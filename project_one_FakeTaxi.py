def not_gate(elem):
	if elem == 1:
		return 0
	elif elem ==0:
		return 1

def and_gate(elem_one, elem_two):
	if elem_one == 1 and elem_two == 1:
		return 1
	else:
		return 0



def or_gate(elem_one, elem_two):
	if elem_one == 1 or elem_two == 1:
		return 1
	else:
		return 0 


def xor_gate(elem_one, elem_two):
	if elem_one == elem_two:
		return 0
	else:
		return 1

def half_adder(elem_one, elem_two):
#	""" summ: right bit of elem_one+elem_two
#		carry: left bit of elem_one+elem_two"""
		summ = xor_gate(elem_one,elem_two)
		carry = and_gate(elem_one,elem_two)
		return summ, carry


def full_adder(elem_one, elem_two, carry):
#	""" summ: right bit of elem_one+elem_two+carry
#		carry: left bit of elem_one+elem_two+carry"""
		rsum, carry1 = half_adder(elem_one,elem_two)
		summ, carry2 = half_adder(rsum, carry)
		fcarry = or_gate(carry1, carry2)
		
		return summ, fcarry  



def alu(elem_one, elem_two, flag):
	"""
	flag is the operation code:
	"""

	elem_one_list = []
	elem_two_list = []
	for digit in elem_one:
		elem_one_list.append(int(digit))
	for digit in elem_two:
		elem_two_list.append(int(digit))
	if flag == "100":
	# not 
		return [not_gate(bit) for bit in elem_one_list], [not_gate(bit) for bit in elem_two_list]
	elif flag == "101":
	# and
		output = []
		i = 0
		for bit in elem_one:
			output.append(and_gate(elem_one_list[i], elem_two_list[i]))
			i += 1
		return output
	elif flag == "110":
	# or
		output = []
		i = 0
		for bit in elem_one:
			output.append(or_gate(elem_one_list[i], elem_two_list[i]))
			i += 1
		return output
	elif flag == "111":
	# xor
		output = []
		i = 0
		for bit in elem_one:
			output.append(xor_gate(elem_one_list[i], elem_two_list[i]))
			i += 1
		return output
	elif flag == "000":
	# add
		elem_one_list.reverse()
		elem_two_list.reverse()
		summ = [0,0,0,0]
		summ[0], carry0 = half_adder(elem_one_list[0],elem_two_list[0])
		print (summ)
		summ[1], carry1 = full_adder(elem_one_list[1],elem_two_list[1], carry0)
		print (summ)
		summ[2], carry2 = full_adder(elem_one_list[2],elem_two_list[2], carry1)
		print (summ)
		summ[3], carry3 = full_adder(elem_one_list[3],elem_two_list[3], carry2)
		print (summ)
		print (carry3)
		if carry3 == 1:
			return "Overflow"
		else:
			summ.reverse()
			return summ
	elif flag == "001":
	# subtract
		elem_one_list.reverse()
		elem_two_list.reverse()
		subtract = [0,0,0,0]
		subtract[0], carry0 = half_adder(elem_one_list[0],elem_two_list[0])
		print (summ)
		subtract[1], carry1 = full_adder(elem_one_list[1],elem_two_list[1], carry0)
		print (summ)
		subtract[2], carry2 = full_adder(elem_one_list[2],elem_two_list[2], carry1)
		print (summ)
		subtract[3], carry3 = full_adder(elem_one_list[3],elem_two_list[3], carry2)
		print (summ)
		print (carry3)
		if carry3 == 1:
			return "Overflow"
		else:
			summ.reverse()
			return summ
		pass
	elif flag == '010':
	# decrement
		pass
	elif flag == '011':
	# increment
		pass
	else:
		return "Invald Operation"

print (alu("1001","0100","000"))
